#-------------------------------------------------
#
# Project created by QtCreator 2023-07-08T10:03:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = praktika1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    SparseArray.cpp

HEADERS  += mainwindow.h \
    SparseArray.h

FORMS    += mainwindow.ui
