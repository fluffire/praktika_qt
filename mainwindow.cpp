#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <ctime>
#include <fstream>
#include <QIntValidator>
#include <QDoubleValidator>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QLocale curLocale(QLocale("en_EN"));
    QLocale::setDefault(curLocale);
    ui->setupUi(this);
    ui->ArrSize->setValidator(new QIntValidator(1, std::numeric_limits<int>::max(), this));
    ui->DefValue->setValidator(new QDoubleValidator);
    ui->IndexEdit->setValidator(new QIntValidator(0, std::numeric_limits<int>::max(), this));
    ui->DataEdit->setValidator(new QDoubleValidator);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_CreateArrayButton_clicked()
{
    int n = ui->ArrSize->text().toInt();
    double def = ui->DefValue->text().toDouble();
    arr = SparseArray(n, def);
    ui->InfoLabel->setText("Массив создан");
    ui->CreateArrayButton->setText("Пересоздать массив");
    ui->ArrayMethodsBox->setEnabled(true);
}

void MainWindow::on_SetElementButton_clicked()
{
    try {
        int i = ui->IndexEdit->text().toInt();
        double data = ui->DataEdit->text().toDouble();
        arr[i] = data;
        ui->InfoLabel->setText(QString("Задан элемент\n[%1] = %2").arg(QString::number(i), QString::number(data)));
    } catch (invalid_argument& e) {
        QMessageBox messageBox;
        messageBox.critical(0,"Ошибка","Выход за пределы массива");
        messageBox.setFixedSize(500,200);
    }
}

void MainWindow::on_RandElementButton_clicked()
{
    std::srand(time(0));
    int n = arr.get_length();
    for (int i = 0; i < n; ++i) {
        arr[rand() % n] += 1;
    }
    ui->InfoLabel->setText("Массив случайно заполнен");
}

void MainWindow::on_GetLengthButton_clicked()
{
    QMessageBox msgBox;
    msgBox.information(0, "Sparse array", "Длина = " + QString::number(arr.get_length()));
    msgBox.setFixedSize(500,200);
}

void MainWindow::on_CountNotEmptyButton_clicked()
{
    QMessageBox msgBox;
    msgBox.information(0, "Sparse array", "Кол-во непустых элементов = " + QString::number(arr.count_not_empty()));
    msgBox.setFixedSize(500,200);
}

void MainWindow::on_PrintNotEmptyButton_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save this"), "", tr("Текстовый документ (*.txt)"));
    if (fileName.isEmpty())
        return;
    ofstream out(fileName.toStdString());
    arr.print_not_empty(out);
    ui->InfoLabel->setText("Непустые элементы сохранены\nв файл");
}

void MainWindow::on_OutputButton_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save this"), "", tr("Текстовый документ (*.txt)"));
    if (fileName.isEmpty())
        return;
    ofstream out(fileName.toStdString());
    out << arr;
    ui->InfoLabel->setText("Массив сохранен в файл");
}

void MainWindow::on_ForEachButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("for_each(func)");
    msgBox.setText("Выберите одно действие");
    QPushButton *x2 = msgBox.addButton(tr("x^2"), QMessageBox::ActionRole);
    QPushButton *xplus1 = msgBox.addButton(tr("x+1"), QMessageBox::ActionRole);
    QPushButton *x10 = msgBox.addButton(tr("x*10"), QMessageBox::ActionRole);
    QPushButton *cancel = msgBox.addButton(tr("Отмена"), QMessageBox::RejectRole);
    msgBox.setDefaultButton(cancel);
    msgBox.setEscapeButton(cancel);
    msgBox.setFixedSize(500,200);
    msgBox.exec();
    if (msgBox.clickedButton() == x2) {
        arr.for_each([](double x){return x*x;});
        ui->InfoLabel->setText("Непустые элементы\nмодифицированы");
    } else if (msgBox.clickedButton() == xplus1) {
        arr.for_each([](double x){return x+1;});
        ui->InfoLabel->setText("Непустые элементы\nмодифицированы");
    } else if (msgBox.clickedButton() == x10) {
        arr.for_each([](double x){return x*10;});
        ui->InfoLabel->setText("Непустые элементы\nмодифицированы");
    }
}

