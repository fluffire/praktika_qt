#include "SparseArray.h"

ostream& operator << (ostream& out, const node& x) {
	out << '[' << x.index << "] " << x.data;
	return out;
}

SparseArray::SparseArray(int n, double def_val) : first(nullptr), last(nullptr) {
	if (n <= 0) n = 1;
	this->n = n;
	this->def_val = def_val;
}

SparseArray::~SparseArray() {
	node* cur = this->first;
	while (cur) {
		this->first = this->first->next;
		delete cur;
		cur = this->first;
	}
}

int SparseArray::get_length() const {
	return n;
}

int SparseArray::count_not_empty() const {
	int count = 0;
	node* cur = first;
	while (cur) {
		count++;
		cur = cur->next;
	}
	return count;
}

void SparseArray::print_not_empty(ostream& out) const {
	node* cur = first;
	while (cur) {
		out << *cur << endl;
		cur = cur->next;
	}
}

void SparseArray::for_each(double (*f) (double)) {
	node* cur = first;
	while (cur) {
		cur->data = f(cur->data);
		cur = cur->next;
	}
}

double& SparseArray::operator [] (int i) {
	if (i < 0 || i >= n)
		throw invalid_argument("Index out of range");
	node* res = nullptr;
	if (!first) { //��� ������� ������ (first==last==nullptr)
		res = first = last = new node(i, def_val, nullptr, nullptr);
	}
	else if (i < first->index) { //������� � ������
		res = this->first = this->first->prev = new node(i, def_val, nullptr, this->first);
	}
	else if (i > last->index) { //������� � �����
		res = this->last->next = new node(i, def_val, this->last, nullptr);
		this->last = this->last->next;
	}
	else { //����� ������
		node* cur = this->first;
		while (cur && !res) {
			if (i == cur->index) {
				res = cur;
			}
			else if (i < cur->index) {
				res = new node(i, def_val, cur->prev, cur);
				cur->prev->next = res;
				cur->prev = res;
			}
			cur = cur->next;
		}
	}
	return res->data;
}

double SparseArray::operator [] (int i) const {
	if (i < 0 || i >= n)
		throw invalid_argument("Index out of range");
	double res = def_val;
	if (first && i >= first->index && i <= last->index) {
		node* cur = this->first;
		while (cur && res == def_val) {
			if (i == cur->index) {
				res = cur->data;
			}
			cur = cur->next;
		}
	}
	return res;
}

ostream& operator << (ostream& out, const SparseArray& arr) { //�����
	node* cur = arr.first;
	for (int i = 0; i < arr.n; ++i) {
		if (cur && cur->index == i) {
			out << '[' << i << "] " << cur->data << endl;
			cur = cur->next;
		}
		else {
			out << '[' << i << "] " << arr.def_val << endl;
		}
	}
	return out;
}
