#pragma once
#include <iostream>
using namespace std;

struct node {
	int index;
	double data;
	node *prev, *next;
	node(int index, double data, node* prev, node* next) {
		this->index = index;
		this->data = data;
		this->prev = prev;
		this->next = next;
	}
	friend 	ostream& operator << (ostream& out, const node& x);
};


class SparseArray {
private:
	int n;
	double def_val;
	node *first, *last;
public:
	SparseArray(int n = 10, double def_val = 0);
	~SparseArray();
	int get_length() const;
	int count_not_empty() const;
	void print_not_empty(ostream& out) const;
    void for_each(double (*f) (double));
	double& operator [] (int i);
	double operator [] (int i) const;
	friend ostream& operator << (ostream& out, const SparseArray& arr);
};
