#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "SparseArray.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_CreateArrayButton_clicked();

    void on_GetLengthButton_clicked();

    void on_CountNotEmptyButton_clicked();

    void on_PrintNotEmptyButton_clicked();

    void on_OutputButton_clicked();

    void on_SetElementButton_clicked();

    void on_RandElementButton_clicked();

    void on_ForEachButton_clicked();

private:
    Ui::MainWindow *ui;
    SparseArray arr;
};

#endif // MAINWINDOW_H
